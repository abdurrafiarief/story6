from django.urls import path, re_path
from .views import *

app_name = "homepage"

urlpatterns = [
    path('', post_status, name="home"),
    path('post_status', post_status, name='post')
]