from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.http import HttpRequest
from .views import *
from django.urls import resolve
from .models import Status
from .urls import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

# Create your tests here.

class home_page_tests(TestCase):
    def test_homepage_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_post_url_is_exist(self):
        response = Client().get('/post_status')
        self.assertEqual(response.status_code, 200)

    def test_models(self):
        Status.objects.create(statusText="test")
        
        request = HttpRequest()
        response = post_status(request)
        html_response = response.content.decode('utf8')
        self.assertIn('test', html_response)




# Create your tests here.
class Story6FunctionalTest(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(
            chrome_options=chrome_options,
            executable_path='./chromedriver')

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

       
    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        self.selenium.get(self.live_server_url + '/')
        
        # find the form and button element
        selenium.implicitly_wait(10)
        
        form = selenium.find_element_by_name("statusForm")

        # Fill the form with data
        form.send_keys("I'm doing fine")
        selenium.implicitly_wait(10)
        # submitting the form
        form.submit()
        selenium.implicitly_wait(10)
        self.assertIn("fine", self.selenium.page_source)
        


        
        

