from django.db import models

# Create your models here.

class Status(models.Model):
    statusText = models.CharField(max_length=500)
    currentTime = models.TimeField(auto_now_add=True)