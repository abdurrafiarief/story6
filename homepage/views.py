from django.shortcuts import render, HttpResponseRedirect, redirect
from django.http import HttpResponse

from .models import Status
from .forms import StatusForm
# Create your views here.


def post_status(request):

    if request.method == "POST":
        form = StatusForm(request.POST)

        if form.is_valid():
            status = Status(statusText = form.data['statusForm'])
            status.save()

            return HttpResponseRedirect('post_status')
    data = Status.objects.all()
    form = StatusForm()
    context = {
        'CONTENT' : data,
        'form' : form
    }

    return render(request, 'homepage.html', context)



