from django import forms

from .models import Status

class StatusForm(forms.Form):
    statusForm = forms.CharField(label="Status", help_text="Please input your status")
